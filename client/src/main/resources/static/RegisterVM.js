function RegisterVM() {
    var self = this;
    self.email = ko.observable();
    self.password = ko.observable();




    self.register = function () {
       
                var usr = {
             
                    email: self.email(),
                    pass: self.password(),
                    rol: {
                        idRole: 1
                    }
                }
                $.ajax({
                    type: "POST",
                    url: "user/register",
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    data: JSON.stringify(usr),
                    success: function (result) {
                        swal({
		                    title: "The registration was successful!",
		                    type: 'success',
		                    onClose: () => {
						    window.location.assign("/login"); 
						  }
                   		 });
                    },
                    error: function(errors) {
                        swal({
		                    title: errors.responseJSON.message,
		                    type: "error"
                    });         
            }
                });
                
                self.email("");
                self.password("");
            }
            
  
}


