function AdminVM() {
    var self = this;
    self.packageArray = ko.observableArray();
    self.cityArray = ko.observableArray();
    self.userArray = ko.observableArray();
    
    self.id;

    self.packageNameToBeAdded = ko.observable();
  
    
    self.destCityToBeAdded = ko.observable();
    self.senderCityToBeAdded = ko.observable();
    self.senderToBeAdded = ko.observable();
    self.receiverToBeAdded = ko.observable();
    
    self.cityToBeAdded = ko.observable();
    self.time = ko.observable();
    
    
    self.openEditablePackage = ko.observable(false);
    self.openEditableRoute = ko.observable(false);

    
    self.load = function() {
    	console.log("get them all");
    	 $.ajax({
             type: "GET",
             url: "admin/packages",
             dataType: "json",
             success: function (data) {
            	 console.log(data);
                 self.packageArray.removeAll();
                 data.forEach(function(entry) {
                     self.packageArray.push(new PackageViewModel(entry));               
                  });
             },
             fail: function(data) {
            	 console.log("call failed");
             }
         });
        self.getCities();
        self.getUsers();
    }
    

    
    self.addPackage = function() {
        var package = {
            name : self.packageNameToBeAdded(),
            sender : {idUser : self.senderToBeAdded().id},
            receiver : {idUser : self.receiverToBeAdded().id },
            senderCity : {idCity : self.senderCityToBeAdded().id},
            destCity : {idCity : self.destCityToBeAdded().id}
        }
        console.log(package);
        $.ajax({
            type: "POST",
            url: "admin/package",
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            data : JSON.stringify(package),
            success: function (data) {
                self.packageArray.push(new PackageViewModel(data));
                swal({
                    title: "The package was added successfully!",
                    icon: 'success',
                    onClose: () => {window.location.assign("/admin");
				         }
                    });
               
                self.openEditablePackage(false);
            },
            fail: function () {
                swal({
                    title: "Adding the package failed",
                    icon: "error"
                });
            },
            error: function(errors) {
                console.log(errors.responseJSON);
                swal({
                    title: errors.responseJSON.message,
                    icon: "error"
                });         
            }
        });
        
   }
    
    self.deletePackage = function(package) {
    	
        swal({
              title: 'Are you sure?',
              text: "You won't be able to revert this!",
              type: 'warning',
              showCancelButton: true,
              confirmButtonColor: '#3085d6',
              cancelButtonColor: '#d33',
              confirmButtonText: 'Yes, delete it!'
            }).then((result) => {
              if (result.value) {
                    $.ajax({
                        type: "DELETE",
                        url: "admin/delete/package/" + package.id,
                        success: function () {
                            swal({
                                title: "The entity has been deleted successfully!",
                                onClose: () => {
                                 window.location.assign("/admin"); }
                                });
                        },
                        error: function(errors) {   
                                swal({
                                    title: errors.responseJSON.message,
                                    type: "error"
                                });         
                        }
                    });
                }			 
            });
        }
    

    self.registerPackage = function(package) {  
        $.ajax({
            type: "GET",
            url: "admin/register/package/"+package.id,
            dataType: "json",
            success: function (data) {
                console.log(data);
                swal({
                    title: "The package is now registred",
                    icon: "success",
                    onClose: () => {
                        window.location.assign("/admin"); }
                });
            },
            fail: function(data) {
                console.log("call failed");
            },
            error: function(errors) {
                console.log(errors.responseJSON);
                swal({
                    title: errors.responseJSON.message,
                    icon: "error"
                });         
            }
        });
    }
    
    self.updatePackage = function() {
        var route = {
            time : new Date(self.time()).toUTCString(),
            city : {idCity:self.cityToBeAdded().id},
        }
        console.log(route);
        $.ajax({
            type: "PUT",
            url: "admin/update/route/" + self.id,
            contentType: "application/json; charset=utf-8",
            data : JSON.stringify(route),
            success: function () {
                swal({
                    title: "The route was added successfully!",
                    type: 'success'
                    });
                self.openEditableRoute(false);
            },
            fail: function () {
                swal({
                    title: "Adding the route step failed",
                    icon: "error"
                });
            },
            error: function(errors) {
                console.log(errors.responseJSON);
                swal({
                    title: errors.responseJSON.message,
                    type: "error"
                });         
            }
        });
    }

    self.getCities = function() {
        $.ajax({
            type: "GET",
            url: "admin/cities",
            dataType: "json",
            success: function (data) {
                console.log(data);
                self.cityArray.removeAll();
                data.forEach(function(entry) {
                    self.cityArray.push(new CityViewModel(entry));               
                 });
            },
            fail: function(data) {
                console.log("call failed");
            }
        });
    }

    self.getUsers = function() {
        $.ajax({
            type: "GET",
            url: "admin/users",
            dataType: "json",
            success: function (data) {
                console.log(data);
                self.userArray.removeAll();
                data.forEach(function(entry) {
                    self.userArray.push(new UserViewModel(entry));               
                 });
            },
            fail: function(data) {
                console.log("call failed");
            }
        });
    }
    
    self.openAddPackage = function() {
        self.openEditablePackage(true);
        self.packageNameToBeAdded("");
    }
    
    self.openUpdatePackage = function(package) {
    	self.id = package.id; 
        self.openEditableRoute(true);
        self.setRouteTime();
    }
    
    self.setRouteTime = function() {
	    var now = new Date();
		var day = ("0" + now.getDate()).slice(-2);
		var month = ("0" + (now.getMonth() + 1)).slice(-2);
		var today = now.getFullYear()+"-"+(month)+"-"+(day) ;
		var datetime = today+"T"+now.getHours()+":"+now.getMinutes();
	    self.time(datetime);
		$('#routeTime').attr({
					   "min" : datetime,
					   "value" : datetime
					});
	               
	   console.log(self.time());
	 }
    

    self.discardRoute = function () {
        self.openEditableRoute(false);
    }

    self.discardPackage = function () {
        self.openEditablePackage(false);
    }

}

function PackageViewModel(data) {
    var self = this;
    console.log(data);

    self.id = data.idPackage;
    self.sender = data.sender.email;
    self.receiver = data.receiver.email;
    self.name = data.name;
    self.senderCity = data.senderCity.name;
    self.destCity = data.destCity.name;
    self.tracking = data.tracking;
    self.route = data.route;
}

function RoutePairViewModel(data) {
    var self = this;
    console.log(data);

    self.id = data.idRoutePair;
    self.city = data.city.name;
    self.time = ko.observable(data.time);
}

function CityViewModel(data) {
    var self = this;
    console.log(data);

    self.id = data.idCity;
    self.name = data.name;
}

function UserViewModel(data) {
    var self = this;
    console.log(data);

    self.id = data.idUser;
    self.name = data.email;
}

