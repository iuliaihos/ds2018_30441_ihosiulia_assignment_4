function UserVM() {
    var self = this;
    self.packageArray = ko.observableArray();
    self.routeArray = ko.observableArray();
    
    self.packagesVisible = ko.observable(false);
    self.routeVisible = ko.observable(false);
    
    self.keyword = ko.observable("");
 
    
    self.user;

    
    self.getPackages = function() {
    	console.log("get them all");
    	 $.ajax({
             type: "GET",
             url: "user/getPackages",
             dataType: "json",
             success: function (data) {
            	 console.log(data);
                 self.packageArray.removeAll();
                 data.forEach(function(entry) {
                     self.packageArray.push(new PackageViewModel(entry));               
                  });
                 self.packagesVisible(true);
             },
             fail: function(data) {
            	 console.log("call failed");
             }
         });
    }
    

    
    self.checkStatus = function(package) {
    	console.log("get them all");
    	 self.routeArray.removeAll();
   	 $.ajax({
            type: "GET",
            url: "user/check/"+package.id,
            dataType: "json",
            success: function (data) {
           	 console.log("data" + data);
                self.routeArray.removeAll();
                data.forEach(function(entry) {
                    self.routeArray.push(new RoutePairViewModel(entry));               
                 });
                self.routeVisible(true);
            },
            fail: function(data) {
           	 console.log("call failed");
            }
        });
   	
   }
    
    self.search = function() {
    	 $.ajax({
             type: "GET",
             url: "user/search/"+self.keyword(),
             dataType: "json",
             success: function (data) {
            	 console.log(data);
                 self.packageArray.removeAll();
                 data.forEach(function(entry) {
                     self.packageArray.push(new PackageViewModel(entry));               
                  });
                 self.packagesVisible(true);
             },
             fail: function(data) {
            	 console.log("call failed");
             }
         });
    }
    
   
}

function setRouteTime(date) {
    var now = new Date(date);
	var day = ("0" + now.getDate()).slice(-2);
	var month = ("0" + (now.getMonth() + 1)).slice(-2);
	var today = now.getFullYear()+"-"+(month)+"-"+(day) ;
	var datetime = today+" "+now.getHours()+":"+now.getMinutes();
 
   return datetime;
 }

function PackageViewModel(data) {
    var self = this;
    console.log(data);

    self.id = data.idPackage;
    self.sender = data.sender.email;
    self.receiver = data.receiver.email;
    self.name = data.name;
    self.senderCity = data.senderCity.name;
    self.destCity = data.destCity.name;
    self.tracking = data.tracking;
}

function RoutePairViewModel(data) {
    var self = this;
    console.log(data);

    self.id = data.idRoutePair;
    self.city = data.city.name;
   // var date = new Date(data.time);
   // self.time = new Date(date.getTime()+date.getTimezoneOffset()*60*1000).toString();
    self.time = setRouteTime(data.time);
}


