/**
 * RoutePairDTO.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package org.server.ws;

public class RoutePairDTO  implements java.io.Serializable {
    private org.server.ws.CityDTO city;

    private java.lang.Long idRoutePair;

    private org.server.ws.PackageDTO pckg;

    private org.server.ws.RoutePairDTO prevRoutePair;

    private java.util.Calendar time;

    public RoutePairDTO() {
    }

    public RoutePairDTO(
           org.server.ws.CityDTO city,
           java.lang.Long idRoutePair,
           org.server.ws.PackageDTO pckg,
           org.server.ws.RoutePairDTO prevRoutePair,
           java.util.Calendar time) {
           this.city = city;
           this.idRoutePair = idRoutePair;
           this.pckg = pckg;
           this.prevRoutePair = prevRoutePair;
           this.time = time;
    }


    /**
     * Gets the city value for this RoutePairDTO.
     * 
     * @return city
     */
    public org.server.ws.CityDTO getCity() {
        return city;
    }


    /**
     * Sets the city value for this RoutePairDTO.
     * 
     * @param city
     */
    public void setCity(org.server.ws.CityDTO city) {
        this.city = city;
    }


    /**
     * Gets the idRoutePair value for this RoutePairDTO.
     * 
     * @return idRoutePair
     */
    public java.lang.Long getIdRoutePair() {
        return idRoutePair;
    }


    /**
     * Sets the idRoutePair value for this RoutePairDTO.
     * 
     * @param idRoutePair
     */
    public void setIdRoutePair(java.lang.Long idRoutePair) {
        this.idRoutePair = idRoutePair;
    }


    /**
     * Gets the pckg value for this RoutePairDTO.
     * 
     * @return pckg
     */
    public org.server.ws.PackageDTO getPckg() {
        return pckg;
    }


    /**
     * Sets the pckg value for this RoutePairDTO.
     * 
     * @param pckg
     */
    public void setPckg(org.server.ws.PackageDTO pckg) {
        this.pckg = pckg;
    }


    /**
     * Gets the prevRoutePair value for this RoutePairDTO.
     * 
     * @return prevRoutePair
     */
    public org.server.ws.RoutePairDTO getPrevRoutePair() {
        return prevRoutePair;
    }


    /**
     * Sets the prevRoutePair value for this RoutePairDTO.
     * 
     * @param prevRoutePair
     */
    public void setPrevRoutePair(org.server.ws.RoutePairDTO prevRoutePair) {
        this.prevRoutePair = prevRoutePair;
    }


    /**
     * Gets the time value for this RoutePairDTO.
     * 
     * @return time
     */
    public java.util.Calendar getTime() {
        return time;
    }


    /**
     * Sets the time value for this RoutePairDTO.
     * 
     * @param time
     */
    public void setTime(java.util.Calendar time) {
        this.time = time;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof RoutePairDTO)) return false;
        RoutePairDTO other = (RoutePairDTO) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.city==null && other.getCity()==null) || 
             (this.city!=null &&
              this.city.equals(other.getCity()))) &&
            ((this.idRoutePair==null && other.getIdRoutePair()==null) || 
             (this.idRoutePair!=null &&
              this.idRoutePair.equals(other.getIdRoutePair()))) &&
            ((this.pckg==null && other.getPckg()==null) || 
             (this.pckg!=null &&
              this.pckg.equals(other.getPckg()))) &&
            ((this.prevRoutePair==null && other.getPrevRoutePair()==null) || 
             (this.prevRoutePair!=null &&
              this.prevRoutePair.equals(other.getPrevRoutePair()))) &&
            ((this.time==null && other.getTime()==null) || 
             (this.time!=null &&
              this.time.equals(other.getTime())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getCity() != null) {
            _hashCode += getCity().hashCode();
        }
        if (getIdRoutePair() != null) {
            _hashCode += getIdRoutePair().hashCode();
        }
        if (getPckg() != null) {
            _hashCode += getPckg().hashCode();
        }
        if (getPrevRoutePair() != null) {
            _hashCode += getPrevRoutePair().hashCode();
        }
        if (getTime() != null) {
            _hashCode += getTime().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(RoutePairDTO.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://ws.server.org/", "routePairDTO"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("city");
        elemField.setXmlName(new javax.xml.namespace.QName("", "city"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://ws.server.org/", "cityDTO"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("idRoutePair");
        elemField.setXmlName(new javax.xml.namespace.QName("", "idRoutePair"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "long"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("pckg");
        elemField.setXmlName(new javax.xml.namespace.QName("", "pckg"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://ws.server.org/", "packageDTO"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("prevRoutePair");
        elemField.setXmlName(new javax.xml.namespace.QName("", "prevRoutePair"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://ws.server.org/", "routePairDTO"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("time");
        elemField.setXmlName(new javax.xml.namespace.QName("", "time"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "dateTime"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
