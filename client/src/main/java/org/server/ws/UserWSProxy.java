package org.server.ws;

public class UserWSProxy implements org.server.ws.UserWS {
  private String _endpoint = null;
  private org.server.ws.UserWS userWS = null;
  
  public UserWSProxy() {
    _initUserWSProxy();
  }
  
  public UserWSProxy(String endpoint) {
    _endpoint = endpoint;
    _initUserWSProxy();
  }
  
  private void _initUserWSProxy() {
    try {
      userWS = (new org.server.ws.impl.UserWSImplServiceLocator()).getUserWSImplPort();
      if (userWS != null) {
        if (_endpoint != null)
          ((javax.xml.rpc.Stub)userWS)._setProperty("javax.xml.rpc.service.endpoint.address", _endpoint);
        else
          _endpoint = (String)((javax.xml.rpc.Stub)userWS)._getProperty("javax.xml.rpc.service.endpoint.address");
      }
      
    }
    catch (javax.xml.rpc.ServiceException serviceException) {}
  }
  
  public String getEndpoint() {
    return _endpoint;
  }
  
  public void setEndpoint(String endpoint) {
    _endpoint = endpoint;
    if (userWS != null)
      ((javax.xml.rpc.Stub)userWS)._setProperty("javax.xml.rpc.service.endpoint.address", _endpoint);
    
  }
  
  public org.server.ws.UserWS getUserWS() {
    if (userWS == null)
      _initUserWSProxy();
    return userWS;
  }
  
  public org.server.ws.UserDTO register(org.server.ws.UserDTO arg0) throws java.rmi.RemoteException{
    if (userWS == null)
      _initUserWSProxy();
    return userWS.register(arg0);
  }
  
  public org.server.ws.PackageDTO[] listAllPackagesForClient(org.server.ws.UserDTO arg0) throws java.rmi.RemoteException{
    if (userWS == null)
      _initUserWSProxy();
    return userWS.listAllPackagesForClient(arg0);
  }
  
  public org.server.ws.PackageDTO[] searchPackage(java.lang.String arg0) throws java.rmi.RemoteException{
    if (userWS == null)
      _initUserWSProxy();
    return userWS.searchPackage(arg0);
  }
  
  public org.server.ws.RoutePairDTO[] checkStatus(java.lang.Long arg0) throws java.rmi.RemoteException{
    if (userWS == null)
      _initUserWSProxy();
    return userWS.checkStatus(arg0);
  }
  
  public org.server.ws.UserDTO login(org.server.ws.UserDTO arg0) throws java.rmi.RemoteException{
    if (userWS == null)
      _initUserWSProxy();
    return userWS.login(arg0);
  }
  
  
}