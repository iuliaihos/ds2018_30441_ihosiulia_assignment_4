package org.server.ws;

public class AdminWSProxy implements org.server.ws.AdminWS {
  private String _endpoint = null;
  private org.server.ws.AdminWS adminWS = null;
  
  public AdminWSProxy() {
    _initAdminWSProxy();
  }
  
  public AdminWSProxy(String endpoint) {
    _endpoint = endpoint;
    _initAdminWSProxy();
  }
  
  private void _initAdminWSProxy() {
    try {
      adminWS = (new org.server.ws.impl.AdminWSImplServiceLocator()).getAdminWSImplPort();
      if (adminWS != null) {
        if (_endpoint != null)
          ((javax.xml.rpc.Stub)adminWS)._setProperty("javax.xml.rpc.service.endpoint.address", _endpoint);
        else
          _endpoint = (String)((javax.xml.rpc.Stub)adminWS)._getProperty("javax.xml.rpc.service.endpoint.address");
      }
      
    }
    catch (javax.xml.rpc.ServiceException serviceException) {}
  }
  
  public String getEndpoint() {
    return _endpoint;
  }
  
  public void setEndpoint(String endpoint) {
    _endpoint = endpoint;
    if (adminWS != null)
      ((javax.xml.rpc.Stub)adminWS)._setProperty("javax.xml.rpc.service.endpoint.address", _endpoint);
    
  }
  
  public org.server.ws.AdminWS getAdminWS() {
    if (adminWS == null)
      _initAdminWSProxy();
    return adminWS;
  }
  
  public org.server.ws.PackageDTO add(org.server.ws.PackageDTO arg0) throws java.rmi.RemoteException{
    if (adminWS == null)
      _initAdminWSProxy();
    return adminWS.add(arg0);
  }
  
  public void remove(java.lang.Long arg0) throws java.rmi.RemoteException{
    if (adminWS == null)
      _initAdminWSProxy();
    adminWS.remove(arg0);
  }
  
  public org.server.ws.PackageDTO registerPackage(java.lang.Long arg0) throws java.rmi.RemoteException{
    if (adminWS == null)
      _initAdminWSProxy();
    return adminWS.registerPackage(arg0);
  }
  
  public org.server.ws.UserDTO[] getUsers() throws java.rmi.RemoteException{
    if (adminWS == null)
      _initAdminWSProxy();
    return adminWS.getUsers();
  }
  
  public void updateRoute(java.lang.Long arg0, org.server.ws.RoutePairDTO arg1) throws java.rmi.RemoteException{
    if (adminWS == null)
      _initAdminWSProxy();
    adminWS.updateRoute(arg0, arg1);
  }
  
  public org.server.ws.CityDTO[] getCities() throws java.rmi.RemoteException{
    if (adminWS == null)
      _initAdminWSProxy();
    return adminWS.getCities();
  }
  
  public org.server.ws.PackageDTO[] getAllPackages() throws java.rmi.RemoteException{
    if (adminWS == null)
      _initAdminWSProxy();
    return adminWS.getAllPackages();
  }
  
  
}