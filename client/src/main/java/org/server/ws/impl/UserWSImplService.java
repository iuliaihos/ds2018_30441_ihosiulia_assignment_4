/**
 * UserWSImplService.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package org.server.ws.impl;

public interface UserWSImplService extends javax.xml.rpc.Service {
    public java.lang.String getUserWSImplPortAddress();

    public org.server.ws.UserWS getUserWSImplPort() throws javax.xml.rpc.ServiceException;

    public org.server.ws.UserWS getUserWSImplPort(java.net.URL portAddress) throws javax.xml.rpc.ServiceException;
}
