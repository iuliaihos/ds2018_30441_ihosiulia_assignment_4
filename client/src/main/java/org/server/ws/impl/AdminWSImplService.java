/**
 * AdminWSImplService.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package org.server.ws.impl;

public interface AdminWSImplService extends javax.xml.rpc.Service {
    public java.lang.String getAdminWSImplPortAddress();

    public org.server.ws.AdminWS getAdminWSImplPort() throws javax.xml.rpc.ServiceException;

    public org.server.ws.AdminWS getAdminWSImplPort(java.net.URL portAddress) throws javax.xml.rpc.ServiceException;
}
