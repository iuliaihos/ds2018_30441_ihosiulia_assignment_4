/**
 * AdminWS.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package org.server.ws;

public interface AdminWS extends java.rmi.Remote {
    public org.server.ws.PackageDTO add(org.server.ws.PackageDTO arg0) throws java.rmi.RemoteException;
    public void remove(java.lang.Long arg0) throws java.rmi.RemoteException;
    public org.server.ws.PackageDTO registerPackage(java.lang.Long arg0) throws java.rmi.RemoteException;
    public org.server.ws.CityDTO[] getCities() throws java.rmi.RemoteException;
    public org.server.ws.UserDTO[] getUsers() throws java.rmi.RemoteException;
    public void updateRoute(java.lang.Long arg0, org.server.ws.RoutePairDTO arg1) throws java.rmi.RemoteException;
    public org.server.ws.PackageDTO[] getAllPackages() throws java.rmi.RemoteException;
}
