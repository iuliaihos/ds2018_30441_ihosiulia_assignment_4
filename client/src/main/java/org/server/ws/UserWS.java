/**
 * UserWS.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package org.server.ws;

public interface UserWS extends java.rmi.Remote {
    public org.server.ws.UserDTO register(org.server.ws.UserDTO arg0) throws java.rmi.RemoteException;
    public org.server.ws.PackageDTO[] listAllPackagesForClient(org.server.ws.UserDTO arg0) throws java.rmi.RemoteException;
    public org.server.ws.PackageDTO[] searchPackage(java.lang.String arg0) throws java.rmi.RemoteException;
    public org.server.ws.RoutePairDTO[] checkStatus(java.lang.Long arg0) throws java.rmi.RemoteException;
    public org.server.ws.UserDTO login(org.server.ws.UserDTO arg0) throws java.rmi.RemoteException;
}
