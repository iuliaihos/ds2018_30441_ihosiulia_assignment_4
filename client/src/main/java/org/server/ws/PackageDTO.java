/**
 * PackageDTO.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package org.server.ws;

public class PackageDTO  implements java.io.Serializable {
    private org.server.ws.CityDTO destCity;

    private java.lang.Long idPackage;

    private java.lang.String name;

    private org.server.ws.UserDTO receiver;

    private org.server.ws.RoutePairDTO route;

    private org.server.ws.UserDTO sender;

    private org.server.ws.CityDTO senderCity;

    private boolean tracking;

    public PackageDTO() {
    }

    public PackageDTO(
           org.server.ws.CityDTO destCity,
           java.lang.Long idPackage,
           java.lang.String name,
           org.server.ws.UserDTO receiver,
           org.server.ws.RoutePairDTO route,
           org.server.ws.UserDTO sender,
           org.server.ws.CityDTO senderCity,
           boolean tracking) {
           this.destCity = destCity;
           this.idPackage = idPackage;
           this.name = name;
           this.receiver = receiver;
           this.route = route;
           this.sender = sender;
           this.senderCity = senderCity;
           this.tracking = tracking;
    }


    /**
     * Gets the destCity value for this PackageDTO.
     * 
     * @return destCity
     */
    public org.server.ws.CityDTO getDestCity() {
        return destCity;
    }


    /**
     * Sets the destCity value for this PackageDTO.
     * 
     * @param destCity
     */
    public void setDestCity(org.server.ws.CityDTO destCity) {
        this.destCity = destCity;
    }


    /**
     * Gets the idPackage value for this PackageDTO.
     * 
     * @return idPackage
     */
    public java.lang.Long getIdPackage() {
        return idPackage;
    }


    /**
     * Sets the idPackage value for this PackageDTO.
     * 
     * @param idPackage
     */
    public void setIdPackage(java.lang.Long idPackage) {
        this.idPackage = idPackage;
    }


    /**
     * Gets the name value for this PackageDTO.
     * 
     * @return name
     */
    public java.lang.String getName() {
        return name;
    }


    /**
     * Sets the name value for this PackageDTO.
     * 
     * @param name
     */
    public void setName(java.lang.String name) {
        this.name = name;
    }


    /**
     * Gets the receiver value for this PackageDTO.
     * 
     * @return receiver
     */
    public org.server.ws.UserDTO getReceiver() {
        return receiver;
    }


    /**
     * Sets the receiver value for this PackageDTO.
     * 
     * @param receiver
     */
    public void setReceiver(org.server.ws.UserDTO receiver) {
        this.receiver = receiver;
    }


    /**
     * Gets the route value for this PackageDTO.
     * 
     * @return route
     */
    public org.server.ws.RoutePairDTO getRoute() {
        return route;
    }


    /**
     * Sets the route value for this PackageDTO.
     * 
     * @param route
     */
    public void setRoute(org.server.ws.RoutePairDTO route) {
        this.route = route;
    }


    /**
     * Gets the sender value for this PackageDTO.
     * 
     * @return sender
     */
    public org.server.ws.UserDTO getSender() {
        return sender;
    }


    /**
     * Sets the sender value for this PackageDTO.
     * 
     * @param sender
     */
    public void setSender(org.server.ws.UserDTO sender) {
        this.sender = sender;
    }


    /**
     * Gets the senderCity value for this PackageDTO.
     * 
     * @return senderCity
     */
    public org.server.ws.CityDTO getSenderCity() {
        return senderCity;
    }


    /**
     * Sets the senderCity value for this PackageDTO.
     * 
     * @param senderCity
     */
    public void setSenderCity(org.server.ws.CityDTO senderCity) {
        this.senderCity = senderCity;
    }


    /**
     * Gets the tracking value for this PackageDTO.
     * 
     * @return tracking
     */
    public boolean isTracking() {
        return tracking;
    }


    /**
     * Sets the tracking value for this PackageDTO.
     * 
     * @param tracking
     */
    public void setTracking(boolean tracking) {
        this.tracking = tracking;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof PackageDTO)) return false;
        PackageDTO other = (PackageDTO) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.destCity==null && other.getDestCity()==null) || 
             (this.destCity!=null &&
              this.destCity.equals(other.getDestCity()))) &&
            ((this.idPackage==null && other.getIdPackage()==null) || 
             (this.idPackage!=null &&
              this.idPackage.equals(other.getIdPackage()))) &&
            ((this.name==null && other.getName()==null) || 
             (this.name!=null &&
              this.name.equals(other.getName()))) &&
            ((this.receiver==null && other.getReceiver()==null) || 
             (this.receiver!=null &&
              this.receiver.equals(other.getReceiver()))) &&
            ((this.route==null && other.getRoute()==null) || 
             (this.route!=null &&
              this.route.equals(other.getRoute()))) &&
            ((this.sender==null && other.getSender()==null) || 
             (this.sender!=null &&
              this.sender.equals(other.getSender()))) &&
            ((this.senderCity==null && other.getSenderCity()==null) || 
             (this.senderCity!=null &&
              this.senderCity.equals(other.getSenderCity()))) &&
            this.tracking == other.isTracking();
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getDestCity() != null) {
            _hashCode += getDestCity().hashCode();
        }
        if (getIdPackage() != null) {
            _hashCode += getIdPackage().hashCode();
        }
        if (getName() != null) {
            _hashCode += getName().hashCode();
        }
        if (getReceiver() != null) {
            _hashCode += getReceiver().hashCode();
        }
        if (getRoute() != null) {
            _hashCode += getRoute().hashCode();
        }
        if (getSender() != null) {
            _hashCode += getSender().hashCode();
        }
        if (getSenderCity() != null) {
            _hashCode += getSenderCity().hashCode();
        }
        _hashCode += (isTracking() ? Boolean.TRUE : Boolean.FALSE).hashCode();
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(PackageDTO.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://ws.server.org/", "packageDTO"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("destCity");
        elemField.setXmlName(new javax.xml.namespace.QName("", "destCity"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://ws.server.org/", "cityDTO"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("idPackage");
        elemField.setXmlName(new javax.xml.namespace.QName("", "idPackage"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "long"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("name");
        elemField.setXmlName(new javax.xml.namespace.QName("", "name"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("receiver");
        elemField.setXmlName(new javax.xml.namespace.QName("", "receiver"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://ws.server.org/", "userDTO"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("route");
        elemField.setXmlName(new javax.xml.namespace.QName("", "route"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://ws.server.org/", "routePairDTO"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("sender");
        elemField.setXmlName(new javax.xml.namespace.QName("", "sender"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://ws.server.org/", "userDTO"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("senderCity");
        elemField.setXmlName(new javax.xml.namespace.QName("", "senderCity"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://ws.server.org/", "cityDTO"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("tracking");
        elemField.setXmlName(new javax.xml.namespace.QName("", "tracking"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "boolean"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
