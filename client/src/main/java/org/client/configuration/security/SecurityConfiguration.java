package org.client.configuration.security;




import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;

@EnableGlobalMethodSecurity(prePostEnabled = true)
@EnableWebSecurity
@Configuration
public class SecurityConfiguration extends WebSecurityConfigurerAdapter {
	
    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http.csrf().disable();
        http.authorizeRequests()
        	.antMatchers("/register").permitAll()
        	.antMatchers("/login").permitAll()
        	.antMatchers("/js/base/*").permitAll()
        	.antMatchers("/js/*").permitAll()
        	.antMatchers("/css/*").permitAll()
        	.antMatchers("/user/*").permitAll()
            .anyRequest().authenticated() 
            .and()
            .formLogin()
            	.loginPage("/login")
        		.permitAll()
        		.successHandler( getSuccessHandler());
            
    }

    @Bean
    public CustomSimpleUrlAuthenticationSuccessHandler getSuccessHandler(){
        return new CustomSimpleUrlAuthenticationSuccessHandler();
    }
    
    

    @Override
    protected void configure(AuthenticationManagerBuilder auth) throws Exception {
       auth.authenticationProvider(authenticationProvider);
    }

    
    @Autowired
    public CustomAuthenticationProvider authenticationProvider;
    
    
   
}