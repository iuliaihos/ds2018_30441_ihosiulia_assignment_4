package org.client.configuration.security;

import java.rmi.RemoteException;
import java.util.ArrayList;
import java.util.List;

import org.server.ws.UserDTO;
import org.server.ws.UserWS;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.stereotype.Component;

@Component
public class CustomAuthenticationProvider implements AuthenticationProvider {
 
	@Autowired
  	UserWS userService;  
	
    @Override
    public Authentication authenticate(Authentication authentication) {
 
        String name = authentication.getName();
        String password = authentication.getCredentials().toString();
         
        UserDTO optionalUser = new UserDTO();
        optionalUser.setEmail(name);
        optionalUser.setPass(password);
        
       
               
    	try {
    		 UserDTO responseUser = userService.login(optionalUser);
    		if(responseUser==null)  
    			return null;
    			 
    		else {
    			List<SimpleGrantedAuthority> roles = new ArrayList<>();
    			roles.add(new SimpleGrantedAuthority("ROLE_" + responseUser.getRol().getRoleName()));
    			return new UsernamePasswordAuthenticationToken(name, password, roles);
    		}
    		} catch (RemoteException e) {	
						e.printStackTrace();
					}
			
    	return null;
      
    }
 
    @Override
    public boolean supports(Class<?> authentication) {
        return authentication.equals(
          UsernamePasswordAuthenticationToken.class);
    }
}