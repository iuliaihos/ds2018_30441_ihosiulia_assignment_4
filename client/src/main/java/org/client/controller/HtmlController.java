package org.client.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;


@Controller
public class HtmlController {

	@RequestMapping("/login")
	public String getLoginPage() {
		return "login.html";
	}
	
	@RequestMapping("/user")
	public String getUserPAge() {
		return "user.html";
	}
	
	@RequestMapping("/register")
	public String getRegisterPAge() {
		return "register.html";
	}
	
	@RequestMapping("/admin")
	public String getAdminPage() {
		return "admin.html";
	}
}
