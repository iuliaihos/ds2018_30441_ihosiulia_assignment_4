package org.client.controller.rest;

import java.rmi.RemoteException;
import java.util.Arrays;
import java.util.List;

import org.server.ws.PackageDTO;
import org.server.ws.RoutePairDTO;
import org.server.ws.UserDTO;
import org.server.ws.UserWS;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(value = "user")
public class UserController {

	@Autowired
	UserWS userService;
	
	@PreAuthorize("hasRole('ROLE_USER')")
	@GetMapping("getPackages")
	public List<PackageDTO> listAllPackagesForClient() throws RemoteException {
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		UserDTO user = new UserDTO();
		user.setEmail(auth.getName());
		PackageDTO[] packages = userService.listAllPackagesForClient(user);
		if (packages==null)
			return null;
		return Arrays.asList(packages);	
	}
	
	@PreAuthorize("hasRole('ROLE_USER')")
	@GetMapping("search/{keyword}")
	public List<PackageDTO> search(@PathVariable String keyword) throws RemoteException {
		PackageDTO[] packages = userService.searchPackage(keyword);
		if (packages==null)
			return null;
		return Arrays.asList(packages);	
		
	}
	
	@PreAuthorize("hasRole('ROLE_USER')")
	@GetMapping("check/{id}")
	public List<RoutePairDTO> checkStatus(@PathVariable Long id) throws RemoteException {
		RoutePairDTO[] route = userService.checkStatus(id);
		if(route==null)
			return null;
		return Arrays.asList(route);	
	}
	

	@PostMapping("register") 
	public UserDTO register(@RequestBody UserDTO user) throws RemoteException {
			return userService.register(user);
	}

}