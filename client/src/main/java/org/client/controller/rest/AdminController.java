package org.client.controller.rest;

import java.rmi.RemoteException;
import java.util.Arrays;
import java.util.List;

import org.server.ws.AdminWS;
import org.server.ws.CityDTO;
import org.server.ws.PackageDTO;
import org.server.ws.RoutePairDTO;
import org.server.ws.UserDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(value = "admin")
public class AdminController {
	
	@Autowired
	AdminWS adminService;
	
	@PreAuthorize("hasRole('ROLE_ADMIN')")
	@PostMapping("package")
	public PackageDTO add(@RequestBody PackageDTO packageDTO) throws RemoteException {
		System.out.println(packageDTO);
		return adminService.add(packageDTO);
	}
	
	@PreAuthorize("hasRole('ROLE_ADMIN')")
	@DeleteMapping("delete/package/{id}")
	public void remove(@PathVariable Long id) throws RemoteException {
		adminService.remove(id);
	}
	
	@PreAuthorize("hasRole('ROLE_ADMIN')")
	@GetMapping("register/package/{id}")
	public PackageDTO registerPackage(@PathVariable Long id) throws RemoteException {
		return adminService.registerPackage(id);
	}
	
	@PreAuthorize("hasRole('ROLE_ADMIN')")
	@PutMapping("update/route/{id}")
	public void updateRoute(@PathVariable Long id, @RequestBody RoutePairDTO pair) throws RemoteException {
		adminService.updateRoute(id, pair);
	}
	
	@PreAuthorize("hasRole('ROLE_ADMIN')")
	@GetMapping("packages")
	public List<PackageDTO> getAllPackages() throws RemoteException {
		PackageDTO[] packages = adminService.getAllPackages();
		if (packages == null)
			return null;
		return Arrays.asList(packages);
	}
	
	@PreAuthorize("hasRole('ROLE_ADMIN')")
	@GetMapping("cities")
	public List<CityDTO> getAllCities() throws RemoteException {
		CityDTO[] cities = adminService.getCities();
		if (cities == null)
			return null;
		return Arrays.asList(cities);
	}
	
	@PreAuthorize("hasRole('ROLE_ADMIN')")
	@GetMapping("users")
	public List<UserDTO> getAllUsers() throws RemoteException {
		UserDTO[] users = adminService.getUsers();
		System.out.println(users);
		if (users == null)
			return null;
		return Arrays.asList(users);
	}
	

}
