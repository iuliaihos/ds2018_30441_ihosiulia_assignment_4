package org.client;

import javax.xml.rpc.ServiceException;

import org.server.ws.AdminWS;
import org.server.ws.UserWS;
import org.server.ws.impl.AdminWSImplService;
import org.server.ws.impl.AdminWSImplServiceLocator;
import org.server.ws.impl.UserWSImplService;
import org.server.ws.impl.UserWSImplServiceLocator;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

/**
 * Hello world!
 *
 */
@SpringBootApplication
public class App {

	/**
	 * Deploys the Spring application inside an embedded Tomcat
	 *
	 * @param args
	 *            args
	 */
	public static void main(String[] args) {
		SpringApplication.run(App.class, args);
	}
	
	@Bean
	UserWS getUserService(){
		    UserWSImplService userImplService = new UserWSImplServiceLocator();
	    	try {
    		
				return userImplService.getUserWSImplPort();
			} catch (ServiceException e) {
				e.printStackTrace();
			}
	    return null;
	}
	
	@Bean
	AdminWS getAdminService(){
		    AdminWSImplService adminImplService = new AdminWSImplServiceLocator();
	    	try {
    		
				return adminImplService.getAdminWSImplPort();
			} catch (ServiceException e) {
				e.printStackTrace();
			}
	    return null;
	}
}
