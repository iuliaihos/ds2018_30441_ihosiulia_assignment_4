/**
 * AdminWS.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package org.server.ws;

import java.util.List;

import javax.jws.WebMethod;
import javax.jws.WebService;

import org.server.dto.CityDTO;
import org.server.dto.PackageDTO;
import org.server.dto.RoutePairDTO;
import org.server.dto.UserDTO;

@WebService
public interface AdminWS {
	
	@WebMethod
    public PackageDTO add(PackageDTO pac);
	
	@WebMethod
    public void remove(Long intPackage);
	
	@WebMethod
    public PackageDTO registerPackage(Long intPackage);
	
	@WebMethod
    public List<UserDTO> getUsers();
	
	@WebMethod
    public List<PackageDTO> getAllPackages();
	
	@WebMethod
    public void updateRoute(Long idPackage, RoutePairDTO routePair);
	
	@WebMethod
    public List<CityDTO> getCities();
}
