package org.server.ws.impl;

import java.util.ArrayList;
import java.util.List;

import javax.jws.WebService;

import org.server.dao.CityDAO;
import org.server.dao.PackageDAO;
import org.server.dao.RoutePairDAO;
import org.server.dao.UserDAO;
import org.server.dto.CityDTO;
import org.server.dto.PackageDTO;
import org.server.dto.RoutePairDTO;
import org.server.dto.UserDTO;
import org.server.entity.City;
import org.server.entity.Package;
import org.server.entity.RoutePair;
import org.server.entity.User;
import org.server.mapper.Mapper;
import org.server.ws.AdminWS;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

@Component
@WebService(endpointInterface = "org.server.ws.AdminWS")
public class AdminWSImpl implements AdminWS{
	
	@Autowired
	PackageDAO packageDAO;
	
	@Autowired
	RoutePairDAO routePairDAO;
	
	@Autowired
	UserDAO userDAO;
	
	@Autowired
	CityDAO cityDAO;
	
	@Autowired
	Mapper mapper;

	@Override
	public PackageDTO add(PackageDTO packageDTO) {
		return mapper.convertToDto(packageDAO.save(mapper.convertToEntity(packageDTO)));
	}

	@Override
	public void remove(Long idPackage) {
		packageDAO.deleteById(idPackage);
		
	}
	
	@Override
	public PackageDTO registerPackage(Long idPackage) {
		packageDAO.register(idPackage);

		return new PackageDTO();
		
	}

	@Override
	public void updateRoute(Long idPackage, RoutePairDTO pair) {
		RoutePair latestPair = routePairDAO.getLastRoutePair(idPackage);
		RoutePair p = mapper.convertToEntity(pair);
		if(latestPair == null) {
			if(routePairDAO.getForPackage(idPackage).size()==0)
				p.setPrevRoutePair(null);
			else
				p.setPrevRoutePair(routePairDAO.getForPackage(idPackage).get(0));
		}
		else
			p.setPrevRoutePair(latestPair);
		Package pac = new Package();
		pac.setIdPackage(idPackage);
		p.setPckg(pac);
		routePairDAO.save(p);
	}

	@Override
	public List<PackageDTO> getAllPackages() {
		List<Package> entities = packageDAO.findAll();		
		List<PackageDTO> dtos = new ArrayList<>();
		for(Package el : entities) {
			dtos.add(mapper.convertToDto(el));
		}
		return dtos;
	}

	@Override
	public List<CityDTO> getCities() {
		List<City> entities = cityDAO.findAll();		
		List<CityDTO> dtos = new ArrayList<>();
		for(City el : entities) {
			dtos.add(mapper.convertToDto(el));
		}
		return dtos;
	}

	@Override
	public List<UserDTO> getUsers() {
		List<User> entities = userDAO.findAll();		
		List<UserDTO> dtos = new ArrayList<>();
		for(User el : entities) {
			dtos.add(mapper.convertToDto(el));
		}
		return dtos;
	}
	


}
