package org.server.ws.impl;

import java.util.ArrayList;
import java.util.List;

import javax.jws.WebService;

import org.server.dao.PackageDAO;
import org.server.dao.RoutePairDAO;
import org.server.dao.UserDAO;
import org.server.dto.PackageDTO;
import org.server.dto.RoutePairDTO;
import org.server.dto.UserDTO;
import org.server.entity.Package;
import org.server.entity.RoutePair;
import org.server.entity.User;
import org.server.mapper.Mapper;
import org.server.ws.UserWS;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
@WebService(endpointInterface = "org.server.ws.UserWS")
public class UserWSImpl implements UserWS {
	
	@Autowired
	Mapper mapper;
	
	@Autowired
	PackageDAO packageDAO;
	
	@Autowired
	RoutePairDAO routePairDAO;
	
	@Autowired 
	UserDAO userDAO;

//   @PostConstruct
//   public void init() {
//	   System.out.println("initializing");
//       SpringBeanAutowiringSupport.processInjectionBasedOnCurrentContext(this);
//
//   }
   
	
	@Override
	public List<PackageDTO> listAllPackagesForClient(UserDTO user) {
		User us = userDAO.findByEmail(user.getEmail());
		List<Package> entities = packageDAO.findAllForClient(us.getIdUser());
		List<PackageDTO> dtos = new ArrayList<>();
		for(Package el : entities) {
			dtos.add(mapper.convertToDto(el));
		}
		return dtos;
	}

	@Override
	public List<PackageDTO> searchPackage(String keyword) {
		List<PackageDTO> dtos = new ArrayList<>();
		List<Package> packages = packageDAO.findAll();
		for(Package p : packages) {
			if(p.getName().equals(keyword)) 
				dtos.add(mapper. convertToDto(p));
		}
		return dtos;
	}

	@Override
	public List<RoutePairDTO> checkStatus(Long idPackage) {
		RoutePair pair = routePairDAO.getLastRoutePair(idPackage);
		if(pair == null)
			return null;
		List<RoutePairDTO> dtos = new ArrayList<>();
		dtos.add(mapper.convertToDto(pair));
		while(pair.getPrevRoutePair()!=null) {
			RoutePair rp = routePairDAO.findByIdRoutePair(pair.getPrevRoutePair().getIdRoutePair());
			dtos.add(mapper.convertToDto(rp));
			pair = rp; 
		}
		return dtos;
	}

	@Override
	public UserDTO login(UserDTO userDTO) {
		User user = userDAO.findByEmail(userDTO.getEmail());	
		if (user.getPass().equals(userDTO.getPass())) {
			UserDTO result = mapper.convertToDto(user);
			System.out.println(result);
			return result;
		}
		else return null;
	}

	

	@Override
	public UserDTO register(UserDTO user) {
		return mapper.convertToDto(userDAO.save(mapper.convertToEntity(user)));
	}


}
