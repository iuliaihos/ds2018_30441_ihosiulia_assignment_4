package org.server.ws;

import java.util.List;

import javax.jws.WebMethod;
import javax.jws.WebService;

import org.server.dto.PackageDTO;
import org.server.dto.RoutePairDTO;
import org.server.dto.UserDTO;

@WebService
public interface UserWS {
	
	@WebMethod
	public List<PackageDTO> listAllPackagesForClient(UserDTO user);
	
	@WebMethod
	public List<PackageDTO> searchPackage(String keyword);

	@WebMethod
	public List<RoutePairDTO> checkStatus(Long idPackage); 
	
	@WebMethod
	public UserDTO login(UserDTO user);
	
	@WebMethod
	public UserDTO register(UserDTO user);
	
	

}
