package org.server.entity;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
@Table(name = "city")
public class City {
	
	@Override
	public String toString() {
		return "City [idCity=" + idCity + ", name=" + name + "]";
	}

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long idCity;
	
	@Column(name="name")
	private String name;
	
	@JsonIgnore
	@OneToMany(mappedBy="senderCity")
	private List<Package> senderCities;

	@JsonIgnore
	@OneToMany(mappedBy="destCity")
	private List<Package> destCities;
	
	@JsonIgnore
	@OneToMany(mappedBy="city")
	private List<RoutePair> routeCities;
	
	public List<Package> getSenderCities() {
		return senderCities;
	}

	public void setSenderCities(List<Package> senderCities) {
		this.senderCities = senderCities;
	}

	public List<RoutePair> getRouteCities() {
		return routeCities;
	}

	public void setRouteCities(List<RoutePair> routeCities) {
		this.routeCities = routeCities;
	}

	public List<Package> getDestCities() {
		return destCities;
	}

	public void setDestCities(List<Package> destCities) {
		this.destCities = destCities;
	}

	public String getName() {
		return name;
	}

	public Long getIdCity() {
		return idCity;
	}

	public void setIdCity(Long idCity) {
		this.idCity = idCity;
	}

	public void setName(String name) {
		this.name = name;
	}

}
