package org.server.entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity
@Table(name = "routePair")
public class RoutePair {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long idRoutePair;
	
	@OneToOne
	@JoinColumn(name="idPrevRoutePair", unique=true)
	private RoutePair prevRoutePair;
	
	@OneToOne(mappedBy="prevRoutePair")
	private RoutePair currentRouterPair;
	
	@ManyToOne
	@JoinColumn(name = "idPackage",nullable=false)
	private Package pckg;
	
	@ManyToOne
	@JoinColumn(name="city")
	private City city;
	
	@Column(name="time")
	private Date time;
	
	public RoutePair getCurrentRouterPair() {
		return currentRouterPair;
	}

	public void setCurrentRouterPair(RoutePair currentRouterPair) {
		this.currentRouterPair = currentRouterPair;
	}

	public RoutePair getPrevRoutePair() {
		return prevRoutePair;
	}

	public void setPrevRoutePair(RoutePair prevRoutePair) {
		this.prevRoutePair = prevRoutePair;
	}

	
	
	public Long getIdRoutePair() {
		return idRoutePair;
	}

	public void setIdRoutePair(Long idRoutePair) {
		this.idRoutePair = idRoutePair;
	}


	public City getCity() {
		return city;
	}

	public void setCity(City city) {
		this.city = city;
	}

	public Date getTime() {
		return time;
	}

	public void setTime(Date time) {
		this.time = time;
	}

	
	
	public Package getPckg() {
		return pckg;
	}

	public void setPckg(Package pckg) {
		this.pckg = pckg;
	}

	

}
