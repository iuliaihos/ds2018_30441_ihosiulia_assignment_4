package org.server.entity;

import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name="roles")
public class Role {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long idRole;
	
	
	@Column(name="roleName")
	private String roleName;
	
	@OneToMany(mappedBy= "rol",fetch = FetchType.EAGER,cascade = CascadeType.ALL,orphanRemoval = true)
	private Set<User> usersToRole;
	
	public Role() {
		
	}
	
	public Role(Long id) { 
		idRole = id;
	}

	@Override
	public String toString(){
		return roleName.toUpperCase();
	}

	public Set<User> getUsersToRole() {
		return usersToRole;
	}

	public void setUsersToRole(Set<User> usersToRole) {
		this.usersToRole = usersToRole;
	}

	public Long getIdRole() {
		return idRole;
	}

	public void setIdRole(Long idRole) {
		this.idRole = idRole;
	}

	public String getRoleName() {
		return roleName;
	}

	public void setRoleName(String roleName) {
		this.roleName = roleName;
	}

}
