package org.server.entity;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
@Table(name = "package")
public class Package {
	
	

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long idPackage;
	
	@ManyToOne
	@JoinColumn(name="sender")
	private User sender;
	
	@ManyToOne
	@JoinColumn(name="receiver")
	private User receiver;
	
	@Column(name="name")
	private String name;
	
	@ManyToOne()
	@JoinColumn(name="senderCity")
	private City senderCity;
	
	@ManyToOne
	@JoinColumn(name="destCity")
	private City destCity;
	
	@Column(name="tracking")
	private boolean tracking;
	
	@JsonIgnore
	@OneToMany(cascade = CascadeType.ALL, mappedBy="pckg", orphanRemoval = true)
	private List<RoutePair> route;
	
	public List<RoutePair> getRoute() {
		return route;
	}

	public void setRoute(List<RoutePair> route) {
		this.route = route;
	}

	public void setSenderCity(City senderCity) {
		this.senderCity = senderCity;
	}

	public void setDestCity(City destCity) {
		this.destCity = destCity;
	}

	public Long getIdPackage() {
		return idPackage;
	}

	public void setIdPackage(Long idPackage) {
		this.idPackage = idPackage;
	}

	public User getSender() {
		return sender;
	}

	public void setSender(User sender) {
		this.sender = sender;
	}

	public User getReceiver() {
		return receiver;
	}

	public void setReceiver(User receiver) {
		this.receiver = receiver;
	}

	public City getSenderCity() {
		return senderCity;
	}

	public City getDestCity() {
		return destCity;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	

	public boolean isTracking() {
		return tracking;
	}

	public void setTracking(boolean tracking) {
		this.tracking = tracking;
	}

}
