package org.server.entity;

import java.io.Serializable;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;

@SuppressWarnings("serial")
@Entity
@Table(name="users")

public class User implements Serializable {
	
	@Override
	public String toString() {
		return "User [idUser=" + idUser + ", email=" + email + ", pass=" + pass + ", rol=" + rol + "]";
	}

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long idUser;
	
	@Column(name="email", unique = true)
	private String email;
	
	@Column(name="pass")
	private String pass;
	
	@ManyToOne
	@JoinColumn(name = "idRole", nullable = false)
	private Role rol;
	
	@JsonIgnore
	@OneToMany(mappedBy="sender")
	List<Package> packagesSender;
	
	@JsonIgnore
	@OneToMany(mappedBy="receiver")
	List<Package> packagesReceiver;
	
	
	
	public User(){
		
	}

	public User(String email, String pass, Role rol) {
		super();
		this.email = email;
		this.pass = pass;
		this.rol = rol;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPass() {
		return pass;
	}

	public void setPass(String pass) {
		this.pass = pass;
	}

	public Long getIdUser() {
		return idUser;
	}

	

	public void setIdUser(Long idUser) {
		this.idUser = idUser;
	}

	

	public Role getRol() {
		return rol;
	}

	public void setRol(Role role) {
		this.rol = role;
	}

	
}
