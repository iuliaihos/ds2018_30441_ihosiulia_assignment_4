package org.server.mapper;

import org.modelmapper.ModelMapper;
import org.server.dto.CityDTO;
import org.server.dto.PackageDTO;
import org.server.dto.RoleDTO;
import org.server.dto.RoutePairDTO;
import org.server.dto.UserDTO;
import org.server.entity.City;
import org.server.entity.Package;
import org.server.entity.Role;
import org.server.entity.RoutePair;
import org.server.entity.User;
import org.springframework.stereotype.Component;
@Component
public class Mapper {
	private ModelMapper modelMapper;
	
	public Mapper() { 
		modelMapper = new ModelMapper();
	}
	
	public PackageDTO convertToDto(Package pckg) {
		PackageDTO packageDTO = modelMapper.map(pckg, PackageDTO.class);
	    return packageDTO;
	}
	
	public Package convertToEntity(PackageDTO packageDTO) {
		Package pckg = modelMapper.map(packageDTO, Package.class);
	    return pckg;
	}
	
	public CityDTO convertToDto(City city) {
		CityDTO cityDTO = modelMapper.map(city, CityDTO.class);
	    return cityDTO;
	}
	
	public City convertToEntity(CityDTO cityDTO) {
		City city = modelMapper.map(cityDTO, City.class);
	    return city;
	}
	
	public RoutePairDTO convertToDto(RoutePair routePair) {
		RoutePairDTO routePairDTO = modelMapper.map(routePair, RoutePairDTO.class);
	    return routePairDTO;
	}
	
	public RoutePair convertToEntity(RoutePairDTO routePairDTO) {
		RoutePair routePair = modelMapper.map(routePairDTO, RoutePair.class);
	    return routePair;
	}

	public UserDTO convertToDto(User user) {
		UserDTO userDTO = modelMapper.map(user, UserDTO.class);
		if(user.getRol()!=null)
			userDTO.setRol(new RoleDTO(user.getRol().getIdRole(),user.getRol().getRoleName()));
	    return userDTO;
	}
	
	public User convertToEntity(UserDTO userDTO) {
		User user = modelMapper.map(userDTO, User.class);
		if(userDTO.getRol()!=null)
			user.setRol(new Role(userDTO.getRol().getIdRole()));
	    return user;
	}

}
