package org.server.dao;

import org.server.entity.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;



@Transactional
@Repository
public interface UserDAO extends JpaRepository<User,Long> {
	User findByEmail(String email);
	User findByIdUser(Long id);
}
