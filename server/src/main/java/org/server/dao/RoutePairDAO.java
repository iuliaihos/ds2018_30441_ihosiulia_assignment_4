package org.server.dao;

import java.util.List;

import org.server.entity.RoutePair;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

@Transactional
@Repository
public interface RoutePairDAO extends JpaRepository<RoutePair,Long>{
	@Query("SELECT max(rp) FROM RoutePair rp WHERE rp.pckg.idPackage = ?1 " + 
			"AND rp.prevRoutePair.idRoutePair = " +
			"(SELECT max(rp2.idRoutePair) FROM RoutePair rp2 WHERE rp2.pckg.idPackage = ?1 " + 
			"AND rp.prevRoutePair.idRoutePair=rp2.idRoutePair)")
	public RoutePair getLastRoutePair(Long idPackage);
	
	@Query("SELECT rp FROM RoutePair rp WHERE rp.pckg.idPackage = ?1")
	public List<RoutePair>  getForPackage(Long idPackage);

	public List<RoutePair> findAllByPckgIdPackage(Long idPackage);
	
	public RoutePair findByIdRoutePair(Long id);
}