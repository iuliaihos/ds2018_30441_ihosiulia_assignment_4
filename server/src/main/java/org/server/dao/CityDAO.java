package org.server.dao;

import javax.transaction.Transactional;

import org.server.entity.City;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
@Transactional
public interface CityDAO  extends JpaRepository<City, Long>{

}
