package org.server.dao;

import java.util.List;

import org.server.entity.Package;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

@Transactional
@Repository
public interface PackageDAO extends JpaRepository<Package,Long>{
	
	@Query("SELECT p FROM Package p WHERE p.sender.idUser = ?1 "
			+ "OR p.receiver.idUser = ?1")
	List<Package> findAllForClient(Long id);
	
	@Modifying
	@Query("UPDATE Package SET tracking = true WHERE idPackage = ?1 ")
	void register(Long id);

	Package findByIdPackage(Long id);
}
