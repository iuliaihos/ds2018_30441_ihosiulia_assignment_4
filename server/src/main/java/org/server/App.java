package org.server;

import javax.xml.ws.Endpoint;

import org.server.dto.PackageDTO;
import org.server.ws.impl.AdminWSImpl;
import org.server.ws.impl.UserWSImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;


@SpringBootApplication
public class App {

	/**
	 * Deploys the Spring application inside an embedded Tomcat
	 *
	 * @param args
	 *            args
	 */
	public static void main(String[] args) {
		SpringApplication.run(App.class, args);
	
		Endpoint.publish("http://localhost:4790/ws/admin", adminImpl);
		Endpoint.publish("http://localhost:4790/ws/user", userImpl);
		
//		RoutePairDTO r = new RoutePairDTO();
//		CityDTO c = new CityDTO(); 
//		c.setIdCity(1L);
//		r.setCity(c);
//		r.setTime(new Date());
//	
//		
//		adminImpl.updateRoute(6L, r);
//		adminImpl.updateRoute(6L, r);
//		adminImpl.updateRoute(6L, r);
//		PackageDTO p = new PackageDTO();
//		p.setIdPackage(1L);
		adminImpl.registerPackage(6L);
	}
	
	
	private static UserWSImpl userImpl;
	private static AdminWSImpl adminImpl;
	
	@Autowired 
	public void setUserImpl(UserWSImpl userImpl) {
		App.userImpl = userImpl;
	}
	
	@Autowired 
	public void setAdminImpl(AdminWSImpl adminImpl) {
		App.adminImpl = adminImpl;
	}
}
