package org.server.dto;

import java.io.Serializable;

@SuppressWarnings("serial")
public class CityDTO implements Serializable {
	
	
	private Long idCity;
	private String name;
	
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Long getIdCity() {
		return idCity;
	}

	public void setIdCity(Long idCity) {
		this.idCity = idCity;
	}

}


