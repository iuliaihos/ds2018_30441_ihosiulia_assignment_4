package org.server.dto;

import java.io.Serializable;
import java.util.Date;


@SuppressWarnings("serial")
public class RoutePairDTO implements Serializable {
	

	private Long idRoutePair;
	private RoutePairDTO prevRoutePair;
	private PackageDTO pckg;
	private CityDTO city;
	private Date time;
	
	public RoutePairDTO getPrevRoutePair() {
		return prevRoutePair;
	}

	public void setPrevRoutePair(RoutePairDTO prevRoutePair) {
		this.prevRoutePair = prevRoutePair;
	}	
	
	public PackageDTO getPckg() {
		return pckg;
	}

	public Long getIdRoutePair() {
		return idRoutePair;
	}

	public void setIdRoutePair(Long idRoutePair) {
		this.idRoutePair = idRoutePair;
	}

	public CityDTO getCity() {
		return city;
	}

	public void setCity(CityDTO city) {
		this.city = city;
	}

	public Date getTime() {
		return time;
	}

	public void setTime(Date time) {
		this.time = time;
	}

	public void setPckg(PackageDTO pckg) {
		this.pckg = pckg;
	}


}

