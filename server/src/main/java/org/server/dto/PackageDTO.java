package org.server.dto;

import java.io.Serializable;


@SuppressWarnings("serial")
public class PackageDTO implements Serializable {
	
	@Override
	public String toString() {
		return "PackageDTO [idPackage=" + idPackage + ", sender=" + sender + ", receiver=" + receiver + ", name=" + name
				+ ", senderCity=" + senderCity + ", destCity=" + destCity + ", tracking=" + tracking ;
	}

	private Long idPackage;
	private UserDTO sender;
	private UserDTO receiver;
	private String name;
	private CityDTO senderCity;
	private CityDTO destCity;	
	private boolean tracking;
	private RoutePairDTO route;
	
	public RoutePairDTO getRoute() {
		return route;
	}

	public void setRoute(RoutePairDTO route) {
		this.route = route;
	}

	public void setSenderCity(CityDTO senderCity) {
		this.senderCity = senderCity;
	}

	public void setDestCity(CityDTO destCity) {
		this.destCity = destCity;
	}



	public Long getIdPackage() {
		return idPackage;
	}

	public void setIdPackage(Long idPackage) {
		this.idPackage = idPackage;
	}

	public UserDTO getSender() {
		return sender;
	}

	public CityDTO getSenderCity() {
		return senderCity;
	}

	public CityDTO getDestCity() {
		return destCity;
	}

	public void setSender(UserDTO sender) {
		this.sender = sender;
	}

	public UserDTO getReceiver() {
		return receiver;
	}

	public void setReceiver(UserDTO receiver) {
		this.receiver = receiver;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public boolean isTracking() {
		return tracking;
	}

	public void setTracking(boolean tracking) {
		this.tracking = tracking;
	}

}
