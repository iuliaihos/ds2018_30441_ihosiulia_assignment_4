package org.server.dto;

import java.io.Serializable;

@SuppressWarnings("serial")
public class RoleDTO implements Serializable{
	private Long idRole;
	private String roleName;
	
	public RoleDTO() {
		
	}
	
	public RoleDTO(Long id, String name) {
		idRole = id;
		roleName = name;
	}

	@Override
	public String toString(){
		return roleName.toUpperCase();
	}

	public Long getIdRole() {
		return idRole;
	}

	public void setIdRole(Long idRole) {
		this.idRole = idRole;
	}

	public String getRoleName() {
		return roleName;
	}

	public void setRoleName(String roleName) {
		this.roleName = roleName;
	}

}
