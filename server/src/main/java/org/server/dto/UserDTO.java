package org.server.dto;

import java.io.Serializable;

@SuppressWarnings("serial")
public class UserDTO implements Serializable {
	
	@Override
	public String toString() {
		return "UserDTO [idUser=" + idUser + ", email=" + email + ", pass=" + pass + ", rol=" + rol + "]";
	}


	private Long idUser;
	private String email;
	private String pass;
	private RoleDTO rol;
	
	
	public UserDTO(){
		
	}

	public UserDTO(String email, String pass, RoleDTO rol) {
		this.email = email;
		this.pass = pass;
		this.rol = rol;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPass() {
		return pass;
	}

	public void setPass(String pass) {
		this.pass = pass;
	}

	public Long getIdUser() {
		return idUser;
	}

	
	public void setIdUser(Long idUser) {
		this.idUser = idUser;
	}

	
	public RoleDTO getRol() {
		return rol;
	}


	public void setRol(RoleDTO role) {
		this.rol = role;
	}

	
}
